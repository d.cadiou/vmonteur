#!/bin/bash

# commandes create preview execute (all par defaut sinon -abcdef...)

# Traitement des options 

# Paramètres généraux :
PROJET=projet.csv
TMPCDE=tmp.sh #script temporaire
DESTINATION=montage
NBSEGMENTPERSOURCE=3 #Nbre de répétitions des lignes de source à l'initialisation du fichier de projet
EXTENSION=MP4
SEEKINTERVAL=2 #taille du déplacement en secondes pour les flèches gauche et droite dans ffplay
	#OUTDIR=${!#} #Dossier de stockage du projet (=le dernier argument)

if [[ ! $1 =~ - ]]; then
	CDE=$1 #Commande
	shift
fi

while [ -n "$1" ]; do # while loop starts
	if [[ $1 =~ - ]]; then
		options=$options"$1"
	else
		if [ "$#" -eq 1 ]; then
			OUTDIR="$1"
		fi
	fi
	
	shift
done
echo "Commande:$CDE Options:$options Projet:$OUTDIR"
OUTDIR=$( sed "s|/$||" <<< $OUTDIR ) # Suppression du '/' final eventuel
LISTE=$OUTDIR/$PROJET #fichier listant les extraits à conserver
CONCATLST=$OUTDIR/concat.txt #fichier temporaire pour la concatenation
TFMVECTOR=$OUTDIR/transform_vectors.trf
DEST=$OUTDIR/$DESTINATION #video concaténant tous les extraits (sans l'extension)
LOGFILE=$OUTDIR/suivi.log
echo "Progression, voir $LOGFILE"
FFLOGFILE=$OUTDIR/ffmpeg.log
echo "ffmpeg log, voir $FFLOGFILE"

if [ -z "$CDE" ] || [ -z "$OUTDIR" ]; then
	cat << FIN
	
	Cet utilitaire gère le montage de vidéos à partir des vidéo source du dossier courant
	
	Usage :
		. vmonteur.sh commande [options] nom_du_projet
		
		Les options varient d'une commande à l'autre. 
		
		Commandes possibles :
			* define
				Pour définir le projet (fichier .csv)
				Options :
					* -i (init) creation de la liste d'extraits, nom_du_projet/$PROJET
					* -e (edit) modification de nom_du_projet/$PROJET (avec calc)
					* -p (play) Lecture successive de toutes les sources disponibles dans $( pwd ) ([Q] pour passer à la source suivante)
			* check (facultatif)
					* -c (clean) post-traitement de nom_du_projet/$PROJET (facultatif, car exécuté par la cde 'execute')
					* -p (play) Lecture successive de tous les extraits choisis
			* execute
				Pour exécuter le projet, voire éteindre l'ordi à la fin
				Options :
					* -g (generate) generation des extraits décrits par nom_du_projet/$PROJET
						[-n] avec 'n' = numéro de la ligne du csv à générer
							(exemple : '-g15' ou '-g -15' pour ne générer et stabiliser que la ligne 15 du csv)
					* -m (merge) fusion des extraits (sans stabilisation) dans $DESTINATION.$EXTENSION
					* -s (stab) stabiliser les extraits pour qui ça a été demandé
						[-n] avec 'n' = numéro de la ligne du csv à stabiliser
					* -f (finalize) fusion des extraits (stabilisés ou non) dans $DESTINATION _stab.$EXTENSION
					* -l (low resolution) créer des copies basses résolution de $DESTINATION _stab.$EXTENSION
					* -p (power off) eteindre l'ordinateur à la fin

		Options communes à toutes les commandes :
			* -d (dry-run) exécution factice (les commandes impactantes ou longues ne sont pas exécutées)
			
		Par exemple, '. vmonteur.sh execute -gmsfl15 toto' 
			va effectuer toutes les étapes du projet (sauf l'extinction finale du PC),
			génération et stabilisation ne seront faites que pour la ligne 15 
			(cela suppose que les autres extraits aient été faits lors d'une exécution précédante, 
			sinon les fusions et actions suivantes échoueront)
		
	Notes 
		* Tous les fichiers générés pour le projet sont dans le dossier qui porte son nom
		
	Lecture vidéos
		La lecture des sources et les diverses prévisualisation se fait avec ffplay.
		* [Q] pour fermer (et passer à la vidéo suivante si plusieurs)
		* clic droit dans la vidéo pour se déplacer
		* [espace] pour pause/lire
		* [flèches...] et [page up/down] pour avancer d'un pas plus ou moins grand ($SEEKINTERVAL sec., 1 min., 10 min.)
		Aide complète sur https://ffmpeg.org/ffplay.html#While-playing
		
	Pour plus d'informations sur ce projet, voir le readme.md qui l'accompagne.
		
FIN
	exit
fi

# Traitement des options eventuelles
optDryrun=0
optInit=0 && optEdit=0 && optPlay=0
optClean=0
optGenerate=0 && optMerge=0 && optStab=0 && optFinalize=0 && optLow=0 && optPoweroff=0 && optLigne=0
if [ -n $options ]; then
#	if [ -n "$( egrep 'd' <<< "$options" )" ]; then
	if [[ $options =~ d ]]; then
		optDryrun=1
		echo "Mode dry-run !!!"
	fi
	case $CDE in
		"define")
			if [[ $options =~ i ]] || [ -z $options ]; then optInit=1; fi
			if [[ $options =~ e ]] || [ -z $options ]; then optEdit=1; fi
			if [[ $options =~ p ]] || [ -z $options ]; then optPlay=1; fi
		;;
		"check")
			if [[ $options =~ c ]] || [ -z $options ]; then optClean=1; fi
			if [[ $options =~ p ]] || [ -z $options ]; then optPlay=1; fi
		;;
		"execute")
			if [[ $options =~ g ]] || [ -z $options ]; then optGenerate=1; fi
			if [[ $options =~ m ]] || [ -z $options ]; then optMerge=1; fi
			if [[ $options =~ s ]] || [ -z $options ]; then optStab=1; fi
			if [[ $options =~ f ]] || [ -z $options ]; then optFinalize=1; fi
			if [[ $options =~ l ]] || [ -z $options ]; then optLow=1; fi
			if [[ $options =~ p ]] || [ -z $options ]; then optPoweroff=1; fi
			if [[ $options =~ [0-9] ]]; then optLigne=$(egrep -o "[0-9]*" <<< $options); fi
		;;
		*)
			echo "Commande $CDE inconnue !"
			exit
		;;
	esac
fi


# Paramètres ffmpeg :
DRAWTEXTCFG="fontfile=/usr/share/fonts/truetype/ubuntu/Ubuntu-M.ttf:fontcolor=white:fontsize=72:x=(w-text_w)/2:y=(h-text_h)/2:shadowcolor='black':shadowx=5:shadowy=5"
ACODEC="-acodec aac"
ANULL="-f lavfi -i anullsrc=channel_layout=mono:sample_rate=32000"
VCODEC="-vcodec h264 -vprofile Main -vlevel 4.2 -tune film -pix_fmt yuv420p" #-preset slow 
VCOLOR="-f lavfi -i color=c=black:s=1920x1080:"
VROTATE="transpose=1,transpose=1" 
	#"-map_metadata 0 -metadata:s:v rotate=180" fonctionne par changement des metadatas, donc sans réencodage, mais que si le fichier n'est pas ensuite fusionné avec d'autres
VSPEED="setpts=(1/%f)*PTS"
#Stabilisation : Voir https://github.com/georgmartius/vid.stab
VSTABDETECT="-vf vidstabdetect=result=$TFMVECTOR:stepsize=6:shakiness=1:accuracy=15:mincontrast=0.05"
VSTABTRANSFORM="-vf vidstabtransform=input=$TFMVECTOR:zoom=0:smoothing=10,unsharp=5:5:0.8:3:3:0.4"
TIMING="-r 59.94 -video_track_timescale 60k"

#Mapping des champs dans le fichier de projet :
colType=1
colIn=2
colOut=3
colDebut=4
colFin=5
colStab=6
colVit=7
colRot=8
colComment=9

# Autre constantes
cdeexploration='ls -1htr | egrep -i "\.$EXTENSION"'

initialisation_generale(){
	mkdir -p $OUTDIR
	printf "" > $LOGFILE
	printf "" > $FFLOGFILE
	if [ -e $OUTDIR/.~lock* ]; then
		echo "!!! Attention !!! Fichier csv probablement encore ouvert dans calc !!! FERMEZ LE !!!"
		exit
	fi
}

# Préparation de la liste des lignes du fichier projet
# $1 si non vide = n° de la seule ligne traitée 
initialisation_commandes() {
	completion_projet
	if [ $1 -gt 0 ]; then # si argument non vide
		echo "/!\ Seule la ligne $1 sera traitée." | tee -a $LOGFILE
		sed $1'!d' $LISTE > $TMPCDE # Suppress all lines, except nber $optLigne
	else
		egrep -i "\.$EXTENSION;" $LISTE > $TMPCDE
	fi
}

preparation() {
	#Initialisation du depot Git :
	cd $OUTDIR
	git init && echo "Depot Git initialise" | tee -a $LOGFILE
	cd ..
	
	#Construction d'une liste d'extraits :
	echo "Initialisation du fichier de projet ($LISTE)..."
	cat <<-eof > $LISTE
		'v', 't' ou 'i';nom_fichier ou "texte"; nom_fichier si texte;(nbre sec);(nbre sec.);(0 ou 1);(1 = normal, 0.5 = ralenti /2...);(0,90,180,270);
		;;;;;;
		Type; In; Out; Debut; Fin; Stabilisation; Vitesse; Rotation; Commentaire
		eof
	
	PROG='BEGIN { printf "T;\"Titre de la video\";;0;2;;;;\n"; }
		{
		for (i = 1; i <= '$NBSEGMENTPERSOURCE'; i++){ 
			printf "V;%s;;0;0;0;1;0;(lg)\n", $0; 
		}
		print ";;;;;;;;";
		}'
	bash -c "$cdeexploration" | awk "$PROG" >> $LISTE
	
	echo 'Mise à jour des durées...'
	awk -F ';' '
		length($'$colIn') > 0 {print $'$colIn';}
		' $LISTE | \
		xargs -I{} bash -c 'duree=$(ffprobe {} 2>&1 | egrep "Duration" | egrep -o "[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{2}") && 
		dureesec=$(( $(date -d $duree +%s) - $(date -d "00:00:00" +%s) +1 )) &&
		sed -i "/{}/s/;0;0;0;1;/;0;$dureesec;0;1;/;/{}/s/(lg)/($duree = $dureesec sec.)/" '$LISTE' &&
		echo "{} traité ($duree = $dureesec sec.)"'

	#cat $LISTE # debug	
}

# Post processing du fichier de projet 
completion_projet () {
	gitcommit "avant remise en forme"
	
	# Suppression lignes vides
	sed -i "/^;/d" $LISTE 
	
	# Vérification du contenu
	PROG='BEGIN { FS=";"; 
		OFS=";" 
		printf "Vérification du Fichier '$LISTE'\n";
		colorbegin = "\033[1;31m";
		colorend = "\033[0m";}
	$'$colIn' ~ /.*'$EXTENSION'/ && length($'$colVit') == 0 { erreur = "vitesse indéfinie : "$'$colVit'; }
	$'$colIn' ~ /.*'$EXTENSION'/ && $'$colRot' !~ /(0|180)/ { erreur = "rotation indéfinie : "$'$colRot'; }
	$'$colIn' ~ /.*'$EXTENSION'/ && $'$colStab' !~ /[01]/ { erreur = "stabilisation indéfinie : "$'$colStab'; }
	length($'$colDebut') == 0 { erreur = "début indéfinie : "$'$colDebut'; }
	length($'$colFin') == 0 { erreur = "fin indéfinie : "$'$colFin'; }
	$'$colFin' < $'$colDebut' { erreur = sprintf("fin mal définie (début : %d et fin : %d)", $'$colDebut', $'$colFin'); }
	length(erreur) > 0 { 
		printf "%s!!!! ERREUR commande n° %d : %s !!!!%s\n", colorbegin, NR, erreur, colorend;
		erreur = "";
		}
	END {printf "Vérification finie.\n";}'

	awk "$PROG" $LISTE
	
	# Définition des noms de fichiers de sortie manquant
	PROG=' BEGIN { FS=";"; OFS=";"; i=0 }
		$'$colType' ~ /^(f|F|v|V)/ && length($'$colOut') == 0 { 
			$'$colOut' = substr($'$colIn',0,length($'$colIn')-length("'$EXTENSION'")-1)"."$'$colDebut';
			#if ( $'$colStab' != "0" ) $'$colOut' = $'$colOut'"s";
			if ( $'$colVit' != "1" ) $'$colOut' = $'$colOut'"v";
			if ( $'$colRot' != "0" ) $'$colOut' = $'$colOut'"r"; 
			$'$colOut' = $'$colOut'".'$EXTENSION'";
		}
		$'$colType' ~ /^(t|T)/ && length($'$colOut') == 0 {
			i++;
			$'$colOut' = "Txt"i".'$EXTENSION'";
		}
		$'$colType' ~ /^(i|I)/ && length($'$colOut') == 0 {
			i++;
			$'$colOut' = "Img"i".'$EXTENSION'";
		}
		{print $0 }'
	#cat <<< $PROG #debug
	awk "$PROG" $LISTE > $LISTE.new
	mv $LISTE.new $LISTE
	
	#  Pour la vitesse remplacement des virgules par des points (0,1 => 0.1)
	awk ' BEGIN { FS=";"; OFS=";" }
	$'$colType' ~ /^(f|F|v|V)/ && $'$colVit' != "1" { sub(",", ".", $'$colVit') }
	{print $0 }' $LISTE > $LISTE.new
	mv $LISTE.new $LISTE
	
	# Calcul de la durée totale
	PROG='BEGIN { FS=";"; tps_sec = 0; }
		$'$colType' ~ /^(f|F|v|V|t|T|i|I)/ && $'$colType' !~ /^Type/ {
			duree = 0;
			if (length($'$colDebut') > 0 && length($'$colFin') > 0) {
				duree = $'$colFin' - $'$colDebut' ;
			}
			if ( $'$colVit' > 0 ) { 
				duree = duree / $'$colVit' ; 
			}
			tps_sec = tps_sec + duree; 
			}
		$'$colType' !~ /^Type/ {printf "  ligne %d (duree : %d sec.) : %s, \n", NR, duree, $0; }
		END {printf "Durée totale %d sec. (%f min.)\n", tps_sec, (tps_sec)/60;}'

	awk "$PROG" $LISTE
	
	echo "$LISTE remis en forme" | tee -a $LOGFILE
}	


cdes_lecture_sources() {
	gnome-terminal -- bash -c "tail -f --pid=$$ $FFLOGFILE" #To open a new terminal with ffmpeg log
	xdotool windowminimize $(xdotool getactivewindow) #To minimize current terminal
	bash -c "$cdeexploration" | xargs -I{} echo ffplay -seek_interval $SEEKINTERVAL {} > $TMPCDE
}

cdes_previsualisation() {
	initialisation_commandes
	
	PROG='
		{ 
			vfilt = "";
			i=0;
			astream="";
		}
		$'$colVit' != 1 { 
			i++;
			filts[i] = sprintf("'$VSPEED'", $'$colVit'); 
			astream="-an";
			}
		$'$colRot' != 0 { 
			i++;
			filts[i] = sprintf("'$VROTATE'", $'$colRot'); 
			}
		$'$colVit' != 1 || $'$colRot' != 0 { 
			vfilt = "-vf \""filts[1]; 
			if (i > 1) vfilt = vfilt","filts[2];
			vfilt = vfilt"\"";
			}
		$'$colType' ~ /^(f|F|v|V)/ {
			printf "timeout %d ffplay -autoexit -seek_interval '$SEEKINTERVAL' -ss %f -t %f %s %s %s #%s\n", 
				($'$colFin'-$'$colDebut')/$'$colVit'+1, 
				$'$colDebut', $'$colFin'-$'$colDebut', astream, vfilt, $'$colIn', $'$colComment';
				# /!\ le temps de lecture dysfonctionne quand la vitesse est modifiée
			}'
	
	cat $TMPCDE | awk -F';' "$PROG" > $TMPCDE.2
	mv $TMPCDE.2 $TMPCDE
}

cdes_extraction() {
	initialisation_commandes $optLigne
	
	PROG='
		{ 
			vfilt = "";
			i = 0;
			astream = "";
			codec = "-codec copy";
			timing = "";
			text = substr($'$colIn', 2, length($'$colIn')-2);
		}
		
		# Texte : # \x27 => guillemet simple
		$'$colType' ~ /^(t|T)/ {
			vstream = sprintf("'$VCOLOR'd=%d -t %d -vf drawtext=\"enable=\x27between(t,0,%s)\x27:text=\x27%s\x27:'$DRAWTEXTCFG'\"",
				$'$colFin', $'$colFin', $'$colFin', text);
			astream = "'$ANULL'";
			timing = "'$TIMING'";
			codec = "'$ACODEC' '$VCODEC'"; # codecs audio et video à redefinir
		}	
		
		$'$colType' ~ /^(i|I)/ {
			vstream = sprintf("-i %s -t %d",
				$'$colIn', $'$colFin');
			astream = "'$ANULL'";
			timing = "'$TIMING'";
			codec = "'$ACODEC' '$VCODEC'"; # codecs audio et video à redefinir
		}	
		
		# Video :
		$'$colVit' != 1 { # vitesse
			i++;
			filts[i] = sprintf("'$VSPEED'", $'$colVit'); 
			astream = "'$ANULL'";
			timing = "'$TIMING'";
			codec = "'$ACODEC' '$VCODEC'"; # codecs audio et video à redefinir
		}
		$'$colRot' != 0 { # rotation
			i++;
			filts[i] = sprintf("'$VROTATE'", $'$colRot'); 
			codec = "'$ACODEC' '$VCODEC'"; # codecs audio et video à redefinir
		}
		$'$colVit' != 1 || $'$colRot' != 0 { # chgt de vfilt
			if (i == 1) vfilt = "-vf \""filts[1]"\""; 
			if (i == 2) vfilt = "-vf \""filts[1]","filts[2]"\""; 
		}
		$'$colType' ~ /^(f|F|v|V)/ {
			vstream = sprintf("-ss %s -to %s -i %s %s",
				$'$colDebut', $'$colFin', $'$colIn', vfilt);
		}
		
		# Commun :
		{
			printf "ffmpeg -y %s %s %s %s '$OUTDIR'/%s\n",
				astream, vstream, timing, codec, $'$colOut';
		}'
	cat $TMPCDE | awk -F';' "$PROG" > $TMPCDE.2
	mv $TMPCDE.2 $TMPCDE
}

cdes_stabilisation() {
	initialisation_commandes $optLigne
	
	# Composition des commandes de stabilisation :
	PROG=' $'$colStab' == "1" {printf "ffmpeg -y -i '$OUTDIR'/%s '$VSTABDETECT' -f null -\n", $'$colOut';
		printf "ffmpeg -y -i '$OUTDIR'/%s '$VSTABTRANSFORM' '$VCODEC' -acodec copy '$OUTDIR'/%s.stab.'$EXTENSION'\n", $'$colOut', $'$colOut';}'
	cat $TMPCDE | awk -F';' "$PROG" > $TMPCDE.2
	mv $TMPCDE.2 $TMPCDE
}

cdes_fusion(){
	initialisation_commandes
	PROG='{printf "file \x27%s\x27\n", $'$colOut';}'
	cat $TMPCDE | awk -F';' "$PROG" > $CONCATLST
	echo "Contenu de $CONCATLST :" | tee -a $LOGFILE
	cat $CONCATLST | tee -a $LOGFILE
	echo "ffmpeg -y -f concat -i $CONCATLST -codec copy "$DEST".mp4" > $TMPCDE
}

cdes_fusion_stab(){
	initialisation_commandes
	PROG='$'$colStab' != "1" {printf "file \x27%s\x27\n", $'$colOut';}
		$'$colStab' == "1" { printf "file \x27%s.stab.'$EXTENSION'\x27\n", $'$colOut';}'
	cat $TMPCDE | awk -F';' "$PROG" > $CONCATLST
	echo "Contenu de $CONCATLST :" | tee -a $LOGFILE
	cat $CONCATLST | tee -a $LOGFILE
	echo "ffmpeg -y -f concat -i $CONCATLST -codec copy "$DEST"_stab.mp4" > $TMPCDE
}

cdes_basse_resolution() {
	printf "" > $TMPCDE
	echo "ffmpeg -y -i "$DEST".mp4 -vf scale=hd720 -r 30 -vcodec h264 -acodec copy "$DEST"_720p30.mp4" >> $TMPCDE
	
	echo "ffmpeg -y -i "$DEST"_stab.mp4 -vf scale=hd720 -r 30 -vcodec h264 -acodec copy "$DEST"_stab_720p30.mp4" >> $TMPCDE
	#echo "ffmpeg -y -i "$DEST"_stab.mp4 -vf scale=qhd -r 30 -vcodec h264 -acodec copy "$DEST"_stab_qhd30.mp4" >> $TMPCDE
	echo "ffmpeg -y -i "$DEST"_stab.mp4 -vf scale=hd480 -r 30 -vcodec h264 -acodec copy "$DEST"_stab_480p30.mp4" >> $TMPCDE
	#echo "ffmpeg -y -i "$DEST"_stab.mp4 -vf scale=nhd -r 30 -vcodec h264 -acodec copy "$DEST"_stab_nhd30.mp4" >> $TMPCDE
}

execution_cdes() {
	echo | tee -a $LOGFILE
	echo | tee -a $LOGFILE
	echo | tee -a $LOGFILE
	echo | tee -a $LOGFILE
	echo $1 | tee -a $LOGFILE #le titre
	NBRE=$(wc -l $TMPCDE | egrep -o "[0-9]*")
	echo $(date)" Exécution des $NBRE commandes suivantes :" | tee -a $LOGFILE
	cat -n $TMPCDE | tee -a $LOGFILE
	
	echo | tee -a $LOGFILE
	#suppression commentaires
	cat $TMPCDE | awk -F '#' '{print $1;}' > $TMPCDE.2
	
	#ajout commandes de suivi de progression
	PROG='{printf "echo $(date) Commande n°%d/'$NBRE'... | tee -a '$LOGFILE'\n", NR;
		print "echo '' >> '$FFLOGFILE'";
		print "date >> '$FFLOGFILE'";
		gsub(/\\n/, "\n", $0);
		printf "%s >> '$FFLOGFILE' 2>&1 && printf \"finie\\n\" | tee -a '$LOGFILE' || printf \"ECHEC\\n\" | tee -a '$LOGFILE'\n", $0;}'
	cat $TMPCDE.2 | awk "$PROG" > $TMPCDE.3
	#cat $TMPCDE.3 #debug
	
	if [ $optDryrun == 0 ]; then
		. $TMPCDE.3
	fi
	rm $TMPCDE.2 $TMPCDE.3
}

gitcommit () {
		#Mise a jour eventuelle du depot Git avant modif :
		cd $OUTDIR
		git add *.csv
		msg="$(date)"
		if [ $# > 0 ]
		then
			msg="$msg $1"
		fi
		git commit -q -m "$msg" 2>&1 > /dev/null && echo "Nouveau commit Git" | tee -a $LOGFILE
		cd ..
}

initialisation_generale

if [ $CDE == "define" ]; then
	if [ $optInit == 1 ]; then 
		preparation
	fi

	if [ $optEdit == 1 ]; then 
		gitcommit
		
		#Ouverture de la liste d'extraits :
		echo "Saisir le détails des extraits dans le tableur qui s'ouvre."
		
		soffice --calc --infilter=CSV:59,0,0,1 $LISTE & #Voir https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options
	fi

	if [ $optPlay == 1 ]; then 
		#Lecture de toutes les vidéos :
		cdes_lecture_sources
		execution_cdes "Lecture de toutes les sources"
	fi
	
elif [ $CDE == "check" ]; then
	if [ $optClean == 1 ]; then 
		completion_projet
	fi
	
	if [ $optPlay == 1 ]; then 
		cdes_previsualisation
		execution_cdes 'Prévisualisation'
	fi
	
elif [ $CDE == "execute" ]; then
	echo "Contenu de $LISTE =" >> $LOGFILE
	cat $LISTE >> $LOGFILE
	
	
	if [ $optGenerate == 1 ]; then 
		cdes_extraction
		execution_cdes 'Génération des extraits'
	fi
	
	if [ $optMerge == 1 ]; then 
		cdes_fusion
		execution_cdes "Fusion des extraits vers $DEST.mp4"
	fi
	
	if [ $optStab == 1 ]; then 
		cdes_stabilisation
		execution_cdes 'Stabilisation de certains extraits'
	fi
	
	if [ $optFinalize == 1 ]; then 
		cdes_fusion_stab
		execution_cdes "Fusion des extraits stabilisés vers $DEST_stab.mp4"
	fi
	
	if [ $optLow == 1 ]; then 
		cdes_basse_resolution
		execution_cdes "Copies basse résolution et low frame rate de $DEST_stab.mp4..."
	fi
	
	echo "Fini." >> $LOGFILE
	
	if [ $optPoweroff == 1 ]; then
		echo $(date)" Extinction de l'ordi" >> $LOGFILE
		shutdown now
	fi
	
	echo "Pour visionner les vidéos : "
	ls -1 $OUTDIR | egrep -i ".*$EXTENSION$" | xargs -I{} echo ffplay $OUTDIR/{}
	echo "Fini."
fi
