#!/bin/bash

if [ $# -eq 0 ]; then
	exit
fi
echo "Conversion du fichier $1"

cp $1 $1.initial
echo "Sauvegarde dans $1.initial"

egrep "^(cut|x|cp|text)" $1.initial > $1

PROG='BEGIN { FS=";"; 
	print "Type; In; Out; Debut; Fin; Stabilisation; Vitesse; Rotation; Commentaire"; }
	
	$1 ~ /^text/ { 
		print "T;«"$4"»;;"$2";"$3";0;1;0;"$8;
	}
	
	$1 ~ /^(cut|x)/ { 
		if ( $5 == "stabilisation" ) {
			stab = 1;
		} else {
			stab = 0;
		}
		
		if ( length($6) == 0 ) {
			vit = 1;
		} else {
			vit = $6;
		}
		
		if ( length($7) == 0 ) {
			rot = 0;
		} else {
			rot = $7;
		}
		
		print "F;"$4";;"$2";"$3";"stab";"vit";"rot";"$8;
	}
	
	$1 ~ /^cp/ { 
		if ( $5 == "stabilisation" ) {
			stab = 1;
		} else {
			stab = 0;
		}
		print "F;"$4";;0;?;"stab";1;0;"$8;
	}'
	
awk "$PROG" $1 > $1.new
mv $1.new $1

echo 'Mise à jour des durées...'
	
awk -F ';' ' $5 == "\?" {print $2;}; ' $1 | \
	xargs -I{} bash -c 'duree=$(ffprobe {} 2>&1 | egrep "Duration" | egrep -o "[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{2}") && 
	dureesec=$(( $(date -d $duree +%s) - $(date -d "00:00:00" +%s) +1 )) &&
	sed -i "/{}/s/;?;/;$dureesec;/" '$1

echo "Résultat"
cat $1
