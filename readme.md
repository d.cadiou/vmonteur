**vmonteur** est un script shell qui automatise des taches de montage vidéo à partir d'une description d'opérations à effectuer, inscrites dans un fichier .csv (éditable avec un tableur).

# Fonctionnalités

Il permet :

* de découper des morceaux provenant de multiples vidéos sources (sans réencodage, donc sans perte)
* de modifier ces morceaux :
  * changement de vitesse, 
  * stabilisation d'image, 
  * rotation 180°
* ajout de texte
* ajout d'image fixe
* d'assembler ces morceaux au sein d'une vidéo unique (sans réencodage s'il n'y a que des opérations de découpes et d'assemblage finale)
* de créer des copies basses résolution de la vidéo générée
* d'éteindre l'ordinateur à la fin des taches demandées
* d'éditer confortablement les opérations à effectuer dans Libre Office Calc
* de prévisualiser le futur montage (sans les changements de vitesse, stabilisation d'images et autres traitements éventuels)
* d'exécuter de façon autonome (sans intervention humaine) la totalité du projet, une fois celui-ci défini
* de tracer (fichiers log) le déroulement de l'exécution du projet

# Principe de fonctionnement

*vmonteur* travaille par projet de montage vidéo. Il doit être lancé dans un dossier contenant les vidéos sources. Chaque projet créé par *vmonteur* l'est sous forme d'un sous-dossier portant le nom du projet.

Il fonctionne en 3 phases, chacune correspondant à une nouvelle exécution du script.

## Phase 1 - définition du projet

Toutes les actions demandées à *vmonteur* doivent être inscrites dans un fichier de projet, ***projet.csv***, au sein du sous-dossier du projet. *vmonteur* peut créer ce fichier de projet pour vous, en le préremplissant et peut aussi vous l'ouvrir dans Libre Office Calc.

Il est également possible de lancer en parallèle la visualisation successive de toutes les vidéos sources, afin de renseigner le fichier de projet au fur et à mesure de la visualisation.

Le contenu attendu dans le fichier de projet est expliqué dans les premières lignes de ce fichier, s'il est auto-généré par *vmonteur*.

## Phase 2 - (optionnel) prévisualisation du projet

Il est également possible de lancer la prévisualisation successive des extraits choisis, afin d'avoir un aperçu de la future vidéo finale.

## Phase 3 - Exécution du projet

*vmonteur* peut ensuite exécuter, **sans aucune intervention humaine**, toutes les opérations décrites dans le projet (découpe, traitement des extraits, assemblage, copies basse définition...), voire éteindre l'ordinateur une fois l'ensemble achevé.

# Utilisation

```bash
. vmonteur.sh
```

# Prérequis

*vmonteur* s'appuie sur 

* **ffmpeg** (https://ffmpeg.org/) <= indispensable
* Libre Office **Calc** 
* **Git** (pour le versionning du fichier de projet

## Historique et compatibilités

*vmonteur* a été créé pour monter sous Linux des vidéos issues d'une GoPro. Il a donc été testé avec des vidéos au format principal de cette GoPro (*session 4*) : mp4 encodé en H264 pour la vidéo en 1080p60 et AAC pour l'audio.

**/!\ vmonteur n'a pas été testé avec d'autres formats vidéo**, mais comme **ffmpeg**, lui, fonctionne avec tous types de formats, il est probable que l'outil soit compatible avec d'autres types de vidéos, sans ou avec peu de modifications.

Les réglages de la stabilisation d'images ont été optimisés pour des vidéos de kayak, notamment pour être insensibles au passage de pagaie en gros plan devant l'objectif.

*vmonteur* a été testé avec un shell *bash* sous Ubuntu.
